provider "aws" {
    region = "us-east-2"
} 

module "abc-infra-setup" {
    source = "./modules/iaac"

    vpc_cidr = "172.31.0.0/24"
    vpc_subnet_cidr = "172.31.0.0/28"
    vpc_subnet_az = "us-east-2a"
} 
