terraform {
    backend "s3" {
        bucket = "terraform-bucket-state-bca"
        key = "tfstate/terraform.tfstate"
        region = "us-east-2"
        dynamodb_table = "terraform-state"
        profile = "default"
    }

}

